package com.globalx.sorter.model;

import com.globalx.sorter.util.SorterApplicationUtil;

/**
 * Immutable Name.
 *
 * @author marlonb
 *
 */
public final class Name {
	private String givenName1;
	private String givenName2;
	private String givenName3;// A Name may have up to 3 given names
	private String lastName;
	
	
	public Name(String givenName1, String givenName2, String givenName3, String lastName) {
		this.givenName1 = SorterApplicationUtil.requireValidName(givenName1, "A Name must have at least 1 given name.");
		this.givenName2 = SorterApplicationUtil.toEmptyStringIfNullOrEmpty(givenName2);
		this.givenName3 = SorterApplicationUtil.toEmptyStringIfNullOrEmpty(givenName3);
		this.lastName = SorterApplicationUtil.requireValidName(lastName, "Last name is required.");
	}

	public String getGivenName1() {
		return givenName1;
	}



	public String getGivenName2() {
		return givenName2;
	}



	public String getGivenName3() {
		return givenName3;
	}



	public String getLastName() {
		return lastName;
	}



	@Override
	public String toString() {
		final StringBuilder nameStr = new StringBuilder(this.givenName1);
		nameStr.append(" ");
		if (!SorterApplicationUtil.isNullOrEmpty(this.givenName2)) {
			nameStr.append(this.givenName2);
			nameStr.append(" ");			
		}
		if (!SorterApplicationUtil.isNullOrEmpty(this.givenName3)) {
			nameStr.append(this.givenName3);
			nameStr.append(" ");
		}
		nameStr.append(this.lastName);		

		return nameStr.toString();
	}

	
}

