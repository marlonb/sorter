package com.globalx.sorter.processor;

import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

import com.globalx.sorter.model.Name;
import com.globalx.sorter.reader.FileReader;
import com.globalx.sorter.writer.NameSorterFileWriter;

/**
 * The Sorter<T> implementation. Reads a file containing a list of names. Runs the comparator to sort the set. 
 * Then, sorted names are shown on the console and printed to a file in the current working directory 
 * called sorted-names-list.txt.
 *
 * @author marlonb
 *
 */
@Component
public class NameSorter implements Sorter<Name> {
	private static final Logger LOGGER = LoggerFactory.getLogger(NameSorter.class);
	@Autowired
	private Comparator<Name> nameComparator;
	@Autowired
	private FileReader<Name> fileReader;
	@Autowired
	private NameSorterFileWriter fileWriter;
	

	@Override
	public List<Name> sort(List<Name> unsortedItems) {
		unsortedItems.sort(this.nameComparator);
		
		return unsortedItems;
	}


	@Override
	public void run(ApplicationArguments args) throws Exception {
		final String[] sourceArgs = args.getSourceArgs();
		if (sourceArgs.length == 0 || sourceArgs[0].trim().isEmpty()) {
			throw new IllegalArgumentException("Filename not specified!");
		}
		final List<Name> unsortedNames = this.fileReader.read(sourceArgs[0].trim());
		if (unsortedNames != null && !unsortedNames.isEmpty()) {
			final List<Name> sortedNames = this.sort(unsortedNames);
			this.fileWriter.write(sortedNames);
		} else {
			LOGGER.error("File is empty!");
		}		
	}
}
