package com.globalx.sorter.processor;

import java.util.List;

import org.springframework.boot.ApplicationArguments;

public interface Sorter<T> {
	/**
	 * Returns a sorted collection of T
	 *
	 * @param unsortedItems
	 * @return
	 */
	List<T> sort(List<T> unsortedItems);
	
	void run(ApplicationArguments args) throws Exception;
}
