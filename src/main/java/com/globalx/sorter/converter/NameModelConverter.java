package com.globalx.sorter.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.globalx.sorter.model.Name;

/**
 * The ModelConverter<T> implementation which converts an input String (source) 
 * to a Name object model.
 *
 * @author marlonb
 *
 */
@Component
public class NameModelConverter implements ModelConverter<Name> {
	private static final Logger LOGGER = LoggerFactory.getLogger(NameModelConverter.class);

	@Override
	public Name convert(String source) {
		Name name = null;
		if (source != null && !source.trim().isEmpty()) {
			final String[] names = source.split("\\s+");
			try {
				if (names.length == 2) {
					name = new Name(names[0], "", "", names[1]);
				} else if (names.length == 3) {
					name = new Name(names[0], names[1], "", names[2]);
				} else if (names.length == 4) {
					name = new Name(names[0], names[1], names[2], names[3]);
				}	
			} catch (IllegalArgumentException e) {
				LOGGER.error("Error converting line to Name model: " + source);				
			}									
		}
		return name;
	}

}
