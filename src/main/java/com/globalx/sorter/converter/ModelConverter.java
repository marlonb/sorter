package com.globalx.sorter.converter;

public interface ModelConverter<T> {

	T convert(String source);
}
