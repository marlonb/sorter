package com.globalx.sorter.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class FileReader<T> implements InputReader<T> {

	public FileReader() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public List<T> read(String absoluteFilename) throws IOException {		
		final List<T> modelList;
		try (final Stream<String> lines = Files.lines(Paths.get(absoluteFilename))) {
			modelList = lines					
					.map(line -> {
						return convertToModel(line);						
					})
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
		}
		return modelList;		
	}

	public abstract T convertToModel(String line);

}
