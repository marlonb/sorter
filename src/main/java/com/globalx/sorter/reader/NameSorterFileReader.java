package com.globalx.sorter.reader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.globalx.sorter.converter.ModelConverter;
import com.globalx.sorter.model.Name;


@Component
public class NameSorterFileReader extends FileReader<Name> {
	@Autowired
	private ModelConverter<Name> modelConverter;
	
	@Override
	public Name convertToModel(String line) {
		return this.modelConverter.convert(line);
		
	}

}
