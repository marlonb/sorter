package com.globalx.sorter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

import com.globalx.sorter.model.Name;
import com.globalx.sorter.processor.Sorter;

@SpringBootApplication
@Profile("!test")
public class SorterApplication implements ApplicationRunner {
	@Autowired
	private Sorter<Name> nameSorter;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(SorterApplication.class);
		app.setWebApplicationType(WebApplicationType.NONE);
		app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		this.nameSorter.run(args);		
	}

}

