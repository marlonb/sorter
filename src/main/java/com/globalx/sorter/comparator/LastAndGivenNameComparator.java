package com.globalx.sorter.comparator;

import java.io.Serializable;
import java.util.Comparator;

import org.springframework.stereotype.Component;

import com.globalx.sorter.model.Name;


/**
 * The default Name sorter.
 * Sorting Algorithm:
 * 
 * 1. First sort Names by last names.
 * 2. Then sort Names by the given names. (A Name can have 1-3 given names).
 * 
 * @author marlonb
 *
 */
@Component
public class LastAndGivenNameComparator implements Comparator<Name>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6638224584003241554L;


	
	public int compare(Name name1,  Name name2) {
		// 1. Compare by last names
		int result = name1.getLastName().toLowerCase().compareTo(name2.getLastName().toLowerCase());
		
		// Compare by First Given Names
		if (result == 0) {			
			result = name1.getGivenName1().toLowerCase().compareTo(name2.getGivenName1().toLowerCase());
		}
		
		// Compare by Second Given Names
		if (result == 0) {
			result = name1.getGivenName2().toLowerCase().compareTo(name2.getGivenName2().toLowerCase());
		}
		
		// Compare by Third Given Names
		if (result == 0) {			
			result = name1.getGivenName3().toLowerCase().compareTo(name2.getGivenName3().toLowerCase());
		}
			
		
		return result;
	}

}
