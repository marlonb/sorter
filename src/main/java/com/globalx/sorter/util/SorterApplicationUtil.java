package com.globalx.sorter.util;

public final class SorterApplicationUtil {

	private SorterApplicationUtil() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public static final String toEmptyStringIfNullOrEmpty(String name) {
		if (isNullOrEmpty(name)) {
			return "";
		}
		return name;
	}
	public static final String requireValidName(String name, String message) {
		if (isNullOrEmpty(name)) {
			throw new IllegalArgumentException(message);
		}
		return name;
	}
	
	public static final boolean isNullOrEmpty(String name) {
		if (name == null || name.trim().isEmpty()) {
			return true;
		}
		return false;
	}

}
