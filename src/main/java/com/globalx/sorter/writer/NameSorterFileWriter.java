package com.globalx.sorter.writer;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.globalx.sorter.model.Name;

@Component
public class NameSorterFileWriter extends FileWriter<Name> {
	private static final Logger LOGGER = LoggerFactory.getLogger(NameSorterFileWriter.class);
	private static final String SORTED_NAMES_OUTPUT_FILENAME = "sorted-names-list.txt";
	
	public void write(List<Name> sortedItems) throws IOException {
		final String cwd = System.getProperty("user.dir");
		if (cwd != null) {
			final Path path = Paths.get(cwd, SORTED_NAMES_OUTPUT_FILENAME);
			super.write(path, sortedItems);			
		} else {
			LOGGER.error("Current working directory is undefined.");
		}
		
	}
}
