package com.globalx.sorter.writer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class FileWriter<T> implements Writer<T> {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileWriter.class);
	public void write(Path path, List<T> sortedItems) throws IOException {
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
			LOGGER.info("****************************************************");
			LOGGER.info("Writing sorted items to file: " + path);
			LOGGER.info("****************************************************");
			sortedItems
			.stream()
			.forEachOrdered(item -> {
				try {						
					writer.write(item.toString());
					writer.newLine();
					System.out.println(item);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					LOGGER.info("Exception encountered writing to output file.");
				}
			});
		}	
	}

}
