package com.globalx.sorter;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.globalx.sorter.configuration.SorterAppTestConfiguration;
import com.globalx.sorter.model.Name;
import com.globalx.sorter.processor.Sorter;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = SorterAppTestConfiguration.class)
public class SorterApplicationTests {	
	@Autowired
	private Sorter<Name> nameSorter;
	
	@Test()
	public void contextLoads() {
		assertThat(nameSorter).isNotNull();
	}

}

