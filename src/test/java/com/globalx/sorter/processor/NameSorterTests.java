package com.globalx.sorter.processor;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.globalx.sorter.configuration.SorterAppTestConfiguration;
import com.globalx.sorter.model.Name;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = SorterAppTestConfiguration.class)
public class NameSorterTests {
	@Autowired
	private Sorter<Name> nameSorter;
	
	@Test
	public void nameSorterShouldBeDefined() {
		assertThat(this.nameSorter).isNotNull();		
	}
	
	@Test
	public void shouldSortNames() {
		List<Name> unsortedNames = this.setupNames(				
				new Name("Chickie", "Linz", "Carce", "Abbyss"),
				new Name("Chickie", "Linz", "Carce", "Abbyss"),
				new Name("Chickie", "Linz", "Darcy", "Abbyss"),
				new Name("Madelon", "Kennett", "Ches", "Abbyss")
				
		);
		List<Name> sortedNames = this.nameSorter.sort(unsortedNames);
		assertThat(sortedNames.size()).isEqualTo(4);
		assertThat(sortedNames.get(0).toString()).isEqualTo("Chickie Linz Carce Abbyss");
		assertThat(sortedNames.get(1).toString()).isEqualTo("Chickie Linz Carce Abbyss");
		assertThat(sortedNames.get(2).toString()).isEqualTo("Chickie Linz Darcy Abbyss");
		assertThat(sortedNames.get(3).toString()).isEqualTo("Madelon Kennett Ches Abbyss");
		
		
		unsortedNames = this.setupNames(				
				new Name("Chickie", "Linz", "Carce", "Abboss"),
				new Name("Chickie", "", "", "Abboss"),
				new Name("Chickie", "Linz", "", "Abboss"),
				new Name("Chickie", "Lin", "May", "Abbott"),
				new Name("Chickie", "Linz", "", "Abbosss"),
				new Name("Chickie", "Linz", "", "Abbiss"),
				new Name("Chickie", "Lin", "", "Abbott"),
				new Name("Madelon", "Kennett", "Ches", "Abboss"),
				new Name("Chickie", "Linz", "", "Abbass"),
				new Name("Chickie", "Linz", "", "Abbott")
				
		);
		sortedNames = this.nameSorter.sort(unsortedNames);
		assertThat(sortedNames.size()).isEqualTo(10);
		assertThat(sortedNames.get(0).toString()).isEqualTo("Chickie Linz Abbass");
		assertThat(sortedNames.get(1).toString()).isEqualTo("Chickie Linz Abbiss");
		assertThat(sortedNames.get(2).toString()).isEqualTo("Chickie Abboss");
		assertThat(sortedNames.get(3).toString()).isEqualTo("Chickie Linz Abboss");
		assertThat(sortedNames.get(4).toString()).isEqualTo("Chickie Linz Carce Abboss");
		assertThat(sortedNames.get(5).toString()).isEqualTo("Madelon Kennett Ches Abboss");
		assertThat(sortedNames.get(6).toString()).isEqualTo("Chickie Linz Abbosss");
		assertThat(sortedNames.get(7).toString()).isEqualTo("Chickie Lin Abbott");
		assertThat(sortedNames.get(8).toString()).isEqualTo("Chickie Lin May Abbott");
		assertThat(sortedNames.get(9).toString()).isEqualTo("Chickie Linz Abbott");
	}
	
	private List<Name> setupNames(Name... names) {
		return Arrays.asList(names);
	}

}
