package com.globalx.sorter.configuration;

import java.util.Comparator;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import com.globalx.sorter.comparator.LastAndGivenNameComparator;
import com.globalx.sorter.converter.ModelConverter;
import com.globalx.sorter.converter.NameModelConverter;
import com.globalx.sorter.model.Name;
import com.globalx.sorter.processor.NameSorter;
import com.globalx.sorter.processor.Sorter;
import com.globalx.sorter.reader.FileReader;
import com.globalx.sorter.reader.NameSorterFileReader;
import com.globalx.sorter.writer.NameSorterFileWriter;

@TestConfiguration
public class SorterAppTestConfiguration {
	@Bean
	public Sorter<Name> nameSorter() {
		return new NameSorter();
	}

	@Bean
	public Comparator<Name> nameComparator() {
		return new LastAndGivenNameComparator();
	}
	@Bean
	public FileReader<Name> fileReader() {
		return new NameSorterFileReader();
	}
	@Bean
	public NameSorterFileWriter fileWriter() {
		return new NameSorterFileWriter();
	}
	@Bean
	public ModelConverter<Name> modelConverter() {
		return new NameModelConverter();
	}

}
