package com.globalx.sorter.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SorterApplicationUtilTests {
	
	@Test
	public void shouldReturnEmptyOrInputString() {
		assertThat(SorterApplicationUtil.toEmptyStringIfNullOrEmpty("John Smith")).isEqualTo("John Smith");
		assertThat(SorterApplicationUtil.toEmptyStringIfNullOrEmpty("")).isEqualTo("");
		assertThat(SorterApplicationUtil.toEmptyStringIfNullOrEmpty(null)).isEqualTo("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldRequireValidName() {		
		SorterApplicationUtil.requireValidName("", "message");
	}
	
	@Test()
	public void shouldReturnErrorMessage() {	
		try {
			SorterApplicationUtil.requireValidName("", "Name is required!");
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage().toString()).isEqualTo("Name is required!");
		}
		
	}
	
	@Test()
	public void shouldReturnName() {	
		assertThat(SorterApplicationUtil.requireValidName("John", "Name is required!")).isEqualTo("John");
		
	}
	
	@Test()
	public void shouldReturnFalse() {	
		assertThat(SorterApplicationUtil.isNullOrEmpty("John")).isFalse();
		
	}
	
	@Test()
	public void shouldReturnTrue() {	
		assertThat(SorterApplicationUtil.isNullOrEmpty("")).isTrue();
		assertThat(SorterApplicationUtil.isNullOrEmpty(null)).isTrue();
		
	}
}
