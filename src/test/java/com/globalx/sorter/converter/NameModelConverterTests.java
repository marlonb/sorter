package com.globalx.sorter.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.globalx.sorter.configuration.SorterAppTestConfiguration;
import com.globalx.sorter.model.Name;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = SorterAppTestConfiguration.class)
public class NameModelConverterTests {
	@Autowired
	private ModelConverter<Name> modelConverter;
	
	@Test
	public void modelConverterShouldBeDefined() {
		assertThat(this.modelConverter).isNotNull();		
	}
	
	@Test
	public void shouldConvertToNameModel() {
		Name name1 = this.modelConverter.convert("jon Smith");
		
		assertThat(name1.getGivenName1()).isEqualTo("jon");
		assertThat(name1.getGivenName2()).isEqualTo("");
		assertThat(name1.getGivenName3()).isEqualTo("");
		assertThat(name1.getLastName()).isEqualTo("Smith");
		assertThat(name1.toString()).isEqualTo("jon Smith");
		
		name1 = this.modelConverter.convert("jon ray Smith");
		
		assertThat(name1.getGivenName1()).isEqualTo("jon");
		assertThat(name1.getGivenName2()).isEqualTo("ray");
		assertThat(name1.getGivenName3()).isEqualTo("");
		assertThat(name1.getLastName()).isEqualTo("Smith");
		assertThat(name1.toString()).isEqualTo("jon ray Smith");
		
		name1 = this.modelConverter.convert("jon    ray Charles   Smith");
		
		assertThat(name1.getGivenName1()).isEqualTo("jon");
		assertThat(name1.getGivenName2()).isEqualTo("ray");
		assertThat(name1.getGivenName3()).isEqualTo("Charles");
		assertThat(name1.getLastName()).isEqualTo("Smith");
		assertThat(name1.toString()).isEqualTo("jon ray Charles Smith");
	}
	
	@Test
	public void shouldReturnNullName() {
		Name name1 = this.modelConverter.convert("Smith");
		
		assertThat(name1).isNull();
		
		name1 = this.modelConverter.convert("John Ray Charles Smith Jr.");
		
		assertThat(name1).isNull();
	}

}
