package com.globalx.sorter.comparator;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.globalx.sorter.configuration.SorterAppTestConfiguration;
import com.globalx.sorter.model.Name;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = SorterAppTestConfiguration.class)
public class LastAndGivenNameComparatorTests {
	@Autowired
	private Comparator<Name> nameComparator;
	
	
	@Before
	public void setup() {
		
	}

	@Test
	public void nameComparatorShouldBeDefined() {
		assertThat(this.nameComparator).isNotNull();		
	}
	
	@Test
	public void shouldCompareLastNamesRegardlessOfCasing() {
		Name name1 = new Name("jon", "", "", "Smith");
		Name name2 = new Name("jon", "", "", "smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isEqualTo(0);
		
		name1 = new Name("joN", "", "", "smith");
		name2 = new Name("jon", "", "", "smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isEqualTo(0);
		
		name1 = new Name("jon", "ray", "", "smith");
		name2 = new Name("jon", "Ray", "", "smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isEqualTo(0);
		
		name1 = new Name("jon", "ray", "charles", "smith");
		name2 = new Name("jon", "ray", "cHarles", "smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isEqualTo(0);
	
	}
	
	@Test
	public void shouldCompareLastNamesThenGvienNames() {
		Name name1 = new Name("John", "", "", "Smith");
		Name name2 = new Name("John", "", "", "Smithie");
		
		assertThat(this.nameComparator.compare(name1, name2)).isLessThan(0);
		
		name1 = new Name("John", "", "", "Smithie");
		name2 = new Name("John", "", "", "Smith");
		
		
		assertThat(this.nameComparator.compare(name1, name2)).isGreaterThan(0);
		
		name1 = new Name("John", "", "", "Smith");
		name2 = new Name("John", "", "", "Smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isEqualTo(0);
		
		
		name1 = new Name("John", "Ray", "", "Smith");
		name2 = new Name("John", "Ray", "", "Smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isEqualTo(0);
		
		name1 = new Name("Johny", "", "", "Smith");
		name2 = new Name("John", "", "", "Smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isGreaterThan(0);
		
		name1 = new Name("Johny", "Ray", "", "Smith");
		name2 = new Name("John", "", "", "Smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isGreaterThan(0);
		
		
		name1 = new Name("Johny", "", "", "Smith");
		name2 = new Name("Johny", "Ray", "", "Smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isLessThan(0);
		
		name1 = new Name("Johny", "Ray", "Charles", "Smith");
		name2 = new Name("Johny", "Ray", "", "Smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isGreaterThan(0);
		
		name1 = new Name("Johny", "Ray", "Charles", "Smith");
		name2 = new Name("Johny", "Ray", "Charley", "Smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isLessThan(0);
		
		name1 = new Name("Johny", "", "", "Smith");
		name2 = new Name("Johny", "Ray", "", "Smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isLessThan(0);
		
		name1 = new Name("Johny", "Ray", "Charles", "Smith");
		name2 = new Name("Johny", "", "", "Smith");
		
		assertThat(this.nameComparator.compare(name1, name2)).isGreaterThan(0);
		
	}
}
