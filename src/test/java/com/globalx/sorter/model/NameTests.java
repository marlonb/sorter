package com.globalx.sorter.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class NameTests {

	@Test
	public void shouldReturnNewName() {
		Name name1 = new Name("jon", "", "", "Smith");
		
		assertThat(name1.getGivenName1()).isEqualTo("jon");
		assertThat(name1.getGivenName2()).isEqualTo("");
		assertThat(name1.getGivenName3()).isEqualTo("");
		assertThat(name1.getLastName()).isEqualTo("Smith");
		assertThat(name1.toString()).isEqualTo("jon Smith");
		
		name1 = new Name("jon", null, null, "Smith");
		
		assertThat(name1.getGivenName1()).isEqualTo("jon");
		assertThat(name1.getGivenName2()).isEqualTo("");
		assertThat(name1.getGivenName3()).isEqualTo("");
		assertThat(name1.getLastName()).isEqualTo("Smith");
		assertThat(name1.toString()).isEqualTo("jon Smith");
		
		name1 = new Name("jon", "ray", null, "Smith");
		
		assertThat(name1.getGivenName1()).isEqualTo("jon");
		assertThat(name1.getGivenName2()).isEqualTo("ray");
		assertThat(name1.getGivenName3()).isEqualTo("");
		assertThat(name1.getLastName()).isEqualTo("Smith");
		assertThat(name1.toString()).isEqualTo("jon ray Smith");
		
		name1 = new Name("jon", "ray", "Charles", "Smith");
		
		assertThat(name1.getGivenName1()).isEqualTo("jon");
		assertThat(name1.getGivenName2()).isEqualTo("ray");
		assertThat(name1.getGivenName3()).isEqualTo("Charles");
		assertThat(name1.getLastName()).isEqualTo("Smith");
		assertThat(name1.toString()).isEqualTo("jon ray Charles Smith");
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldRequireGivenName1() {
		Name name1 = new Name("", "Ray", "Charles", "Smith");
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldRequireLastName() {
		Name name1 = new Name("John", "Ray", "Charles", "");
		
	}

}
