# Sorter Application #

## Quick Information ##
The Sorter Application is a stand-alone custom Java Object sorting app. You can implement a specific Sorter by implementing your own Sorter (Sorter<T>),
ModelConverted<T> and Comparator<T>.

## NameSorter ##
The application comes with a default NameSorter implementation. 
Given a set of names (List<Name>), the NameSorter will order that set first by last name, then by any given names the person may have.
A name must have at least 1 given name and may have up to 3 given names.

- Model:
    - Name - The Name object model. A name must have at least 1 given name and may have up to 3 given names.
- Comparator:
    - LastAndGivenNameComparator - The Comparator<T> implementation which compares Name objects by the following properties (if given, in order): 
    lastName, givenName1, givenName2, givenName3.
- Converter:
    - NameModelConverter - The ModelConverter<T> implementation which converts an input String (source) to a Name object model.
- Sorter:
    - NameSorter - The Sorter<T> implementation. Reads a file containing a list of names. Runs the comparator to sort the set.
    Then, sorted names are shown on the console and printed to a file in the current working directory called sorted-names-list.txt.
    
## Framework ##
Spring Boot is used primarily to provide dependency injection to easily plug-in or swap new Sorter, Comparator and converter implementations.
By using Spring Boot, the application can easily integrate with Db and web services later on.

## Example Usage ##
F:\sample\sorter\sorter>java -jar sorter-0.0.1-SNAPSHOT.jar F:\sample\sorter\unsorted-names-list.txt
